package tn.esprit.spring.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Employe implements Serializable {

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private int id;
private String login;
private String password;
private String email;
@Enumerated(EnumType.STRING)
//@NotNull
private Role role;

//Ajouter les Getters & les Setters 
//Ajouter un constructeur sans argument

public String getLogin() {
	return login;
}
public void setLogin(String login) {
	this.login = login;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getEmail() {
	return email;
}
public void setEmail(String email) {
	this.email = email;
}
public Role getRole() {
	return role;
}
public void setRole(Role role) {
	this.role = role;
}
public Employe() {
	super();
}


}
